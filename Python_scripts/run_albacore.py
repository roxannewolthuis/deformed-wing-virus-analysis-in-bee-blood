
import os
import sys

# for each folder in the fast5 folder --> run the command
for file in os.listdir("reads/20180328_1155_first_run/fast5/"):
	command = os.system("read_fast5_basecaller.py --flowcell FLO-MIN107 --kit SQK-RBK004 --barcoding --output_format fast5,fastq --input reads/20180328_1155_first_run/fast5/" + file + " --save_path output_albacore --worker_threads 2")

import sys
import os
from Bio.Blast import NCBIXML
from Bio import SeqIO
import glob
import re
'''
This program will open an xml file and a fastq file from 2 different folders. This will be the porechop output and the blast output
The files will be filtered to find reads with an alignmentscore of 0.8 or higher
All reads will be added to a new fastq file
This program can also work for fasta files, but you have to change everything fastq to fasta
This script can be implemented in the pipeline
'''
class Assembly:
	
	def getfiles(self):
		# Get xml and fasta files and append the files to lists
		self.xmlfiles = []
		self.fastafiles = []
		for self.xml_file in glob.glob('xml/*'):
			self.xmlfiles.append(self.xml_file)
		for self.fasta_file in glob.glob('fastq/*'):
			self.fastafiles.append(self.fasta_file)
	
	def findseq(self):
		# for file in the lists if the name of the files are the same run the sequence parser
		for item in self.xmlfiles:
			for self.i in self.fastafiles:
				if item[4:21] == self.i[6:23]:
					# Create files as result_handle and fasta_result			
					self.result_handle = open(item)
					filename = item[4:21] + "test.fastq"
					file = open(filename,"w") 
					blast_records = NCBIXML.parse(self.result_handle)
					#self.fasta_result = open(i)
					#print(item)
					t = []
					unique_list = []
					# For the record in all the records
					for blast_record in blast_records:
						#print(blast_record.query)
						for alignment in blast_record.alignments:
						  # For the hsp in the hsps of the alignment
							for hsp in alignment.hsps:
								#print(hsp.align_length)
								align_score = hsp.identities / hsp.align_length
								#print(align_score)
								with open(self.i, "r") as handle:
									for record in SeqIO.parse(handle, "fastq"):		
										if align_score > 0.8 and record.description == blast_record.query:
											#print(align_score)	
							#print("de record in fasta bestand is: {} \n de record sequentie in het fasta bestand is: {} \n de blast query  van xml is:{} \n".format(record.description, record.seq, blast_record.query))
											newread = record.format("fastq")
						#	newread = ">{}\n{}\n".format(record.description, record.seq)
											#print(newread)
											t.append(newread)		
												
					for x in t:
						# check if exists in unique_list or not
						if x not in unique_list:
							unique_list.append(x)
					# print list
					for x in unique_list:
						print(x)
						file.write(str(x))
					file.close() 
		
a = Assembly()
a.getfiles()
a.findseq()
					# 				#print(line)
					# 				# Voeg header en seq toe aan dict
					# 				sequencedict = {}
					# 				header = ""
					# 				sequence = ""
									
					# 				if line.startswith(">"):
					# 					header = line.strip(">")
					# 					#header = line
					# 					#print(header[1:])
					# 				else:
					# 					sequence = line.replace("\n", "")
					# 				#print(header[1:])
					# 				if align_score > 0.8:
					# 					print(blast_record.query)
									#if align_score > 0.8 and header[1:] == blast_record.query:
										# print dict item
									#print(sequence)
									#	print(header, sequence)
									#for
									#print(sequence)
import sys
import os
from Bio import SeqIO
import glob
'''This script will filter a fastq file based on the length of a read. 
If the length is 1000 or higher the read will be added to a new file, these reads wil be used for assembly'''

class filter_Reads:
	def find_read_fastq(self):
		for self.fastqfile in glob.glob('barcodes_trimmed/*'):
			filename = self.fastqfile[17:34] + "test.fasta"
			file = open(filename,"w") 
			with open(self.fastqfile, "r") as handle:
				for record in SeqIO.parse(handle, "fasta"):			 
					if len(record.seq) >= 1000:
						file.write(record.format("fasta"))
			file.close()

f = filter_Reads()
f.find_read_fastq()
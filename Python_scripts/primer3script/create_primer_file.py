# Imports and packages needed to run this program
import sys
from Bio import SeqIO
import argparse
import re
#import subprocess
import os
'''
This program will create a .txt file that can be used as input in primer3. An example format of the file is shown below.
The program will also run primer3 with the created file.
As input the program asks for an inputfile with contigs and some settings for primer3. When no setting is given a default setting will be selected.
It is possible to add more options to this script in the future.

Example of the file structure:
PRIMER_TASK=generic
PRIMER_OPT_SIZE=20
PRIMER_MAX_NS_ACCEPTED=1
PRIMER_PRODUCT_SIZE_RANGE=75-100
P3_FILE_FLAG=1
PRIMER_EXPLAIN_FLAG=1
PRIMER_THERMODYNAMIC_PARAMETERS_PATH=/data/storix2/student/Thema12/MinionMinor2018/primer3/primer3-2.4.0/src/primer3_config/
=
SEQUENCE_ID=test1
SEQUENCE_TEMPLATE=GTAGTCAGTAGACNATGACNACTGACGATGCAGACNACACACACACACACAGCACACAGGTATTAGTGGGCCATTCGATCCCGACCCAAATCGATAGCTACGATGACG
=
SEQUENCE_ID=test2
SEQUENCE_TEMPLATE=GTAGTCAGTAGACNATGACNACTGACGATGCAGACNACACACACACACACAGCACACAGGTATTAGTGGGCCATTCGATCCCGACCCAAATCGATAGCTACGATGACG
=
SEQUENCE_ID=test3
SEQUENCE_TEMPLATE=GTAGTCAGTAGACNATGACNACTGACGATGCAGACNACACACACACACACAGCACACAGGTATTAGTGGGCCATTCGATCCCGACCCAAATCGATAGCTACGATGACG
=

This script can be implemented in the pipeline
'''
class Primer3:
	#hier verder werken met laatste functie
	def openfile(self):
		self.file = open("primer3_settings_contigs_file.txt", "w")		
		#print(self.file)
		#print(self.primer_task)
		# Insert the filename with the contigs to be transformed into primers, comes as output from velvet
		self.handle = open("contigs_default_settings_test.fa")

	def parse_arguments(self):
		# argparser
		desc_text = '''This program will create a .txt file that can be used as input in primer3. An example format of the file is shown below.
	The program will also run primer3 with the created file.
	As input the program asks for an inputfile with contigs and some settings for primer3. When no setting is given a default setting will be selected.'''

		parser = argparse.ArgumentParser(description=desc_text, formatter_class=argparse.RawDescriptionHelpFormatter)
		self.primer_task_options = [	"generic", "pick_detection_primers", "check_primers", "pick_primer_list", "pick_sequencing_primers", "pick_cloning_primers",
								"pick_discriminative_primers", "pick_pcr_primers", "pick_pcr_primers_and_hyb_probe", "pick_left_only", "pick_right_only", 
								"pick_hyb_probe_only"]
		# Get primer3 settings						
		parser.add_argument("PRIMER_TASK", help="For all options and an explanation of the options see the primer3 manual. default value: generic", nargs='?', const=1, type=str, default="generic")
		parser.add_argument("PRIMER_OPT_SIZE", help="Insert an optimal primer size. default value: 20", nargs='?', const=1, type=int, default=20)
		parser.add_argument("PRIMER_MAX_NS_ACCEPTED", help="Max number of ns accepted. default value: 1", nargs='?', const=1, type=int, default=1)
		parser.add_argument("PRIMER_PRODUCT_SIZE_RANGE", help="Insert the product size. The product size consists of two numbers with a - in between default value: 75-100", nargs='?', const=1, type=str, default="75-100")
		# Parse arguments
		args = parser.parse_args()
		
		self.primer_task = args.PRIMER_TASK
		self.primer_opt_size = str(args.PRIMER_OPT_SIZE)
		self.primer_max_ns_accepted = str(args.PRIMER_MAX_NS_ACCEPTED)
		self.primer_product_size_range = args.PRIMER_PRODUCT_SIZE_RANGE
		self.p3_file_flag = "1"
		self.primer_explain_flag = "1"
		# Insert primer3 download folder into the pipeline, insert path to primer3_config here 
		self.primer_thermodynamic_parame = "/data/storix2/student/Thema12/MinionMinor2018/primer3/primer3-2.4.0/src/primer3_config/"
		self.divider = "="
		
		# Raise error if the option is incorrect		
		if args.PRIMER_TASK not in self.primer_task_options:
			print("Error, this is not a correct primer task. Choose from:")
			#print(*self.primer_task_options, sep=", ")
		else:
			pass

		# Raise error if the format is incorrect
		regexsize = "[0-9]+-[0-9]+"
		a = re.match(regexsize, args.PRIMER_PRODUCT_SIZE_RANGE)
		if not a:
			print("Error, this is not a correct primer size, please write the primer size like: 75-100 for example")
		else:
			pass

	def setup_primer3_settings(self):
		# Name of the new file that will be created		
		self.file.write(	"PRIMER_TASK=" + self.primer_task + "\n" + 
					"PRIMER_OPT_SIZE=" + self.primer_opt_size + "\n" + 
					"PRIMER_MAX_NS_ACCEPTED=" + self.primer_max_ns_accepted + "\n" + 
					"PRIMER_PRODUCT_SIZE_RANGE=" + self.primer_product_size_range + "\n" + 
					"P3_FILE_FLAG=" + self.p3_file_flag + "\n" + 
					"PRIMER_EXPLAIN_FLAG=" + self.primer_explain_flag + "\n" + 
					"PRIMER_THERMODYNAMIC_PARAMETERS_PATH=" + self.primer_thermodynamic_parame + "\n" + 
					self.divider + "\n")
	#setup_primer3_settings()

	def insert_sequence(self):
		# empty dict
		sequences = {}
		# Add id in dictionary first as key
		# Add sequence as value, handle = file
		for record in SeqIO.parse(self.handle, "fasta"):
			sequences.update({record.description:str(record.seq).upper()})
		# get seqid and sequence from each item in dict into the file 
		for seqid, sequence in sequences.items():
			self.sequence_id = seqid
			self.sequence_template = sequence
			self.file.write(	"SEQUENCE_ID=" + self.sequence_id + "\n" + 
						"SEQUENCE_TEMPLATE=" + self.sequence_template + "\n" + 
						self.divider + "\n")
	#insert_sequence()
		
	#create function to call primer3
	def closefile(self):
		self.file.close()

	def run_primer3(self):
		# Run this program in the output directory since you cant give and output directory?!!?
		
		# Werkt, als je er shell=True bij zet word er alleen ls gedaan en niet ls -l?
		#subprocess.call(["ls", "-l"])

		# Laat de manual van primer3 zien en blijft dan runnen, er word niks geproduceert
		#subprocess.call(["primer3_core", "<", "primer3_settings_contigs_file.txt"])
		#p = subprocess.Popen(["primer3_core < primer3_settings_contigs_file.txt"], stdout=subprocess.PIPE)
			command = os.system("primer3_core < primer3_settings_contigs_file.txt")


p = Primer3()
p.openfile()
p.parse_arguments()
p.setup_primer3_settings()
p.insert_sequence()
p.closefile()
p.run_primer3()
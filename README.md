## MinION pipeline for the metagenomic analysis

* **Author(s):**            Roxanne Wolthuis, Lisette van Dam 
* **Student numbers:**      347958, 313185
* **Class:**                BFV3
* **Course:**               Thema 11 + 12
* **Study:**                Bio-informatics
* **Institute:**            Institute for Lifescience & Technology
* **Lector:**               Dave Langers
* **Commissioned by:**      Martijn Herber
* **Date:**                 06 - 07 - 2018

## Content of this repository
* **Documentation:**
A folder with all files for the documentation of this project and an appendix folder with all the appendixes for the final report.
* Final_report.pdf
	* The final report of this project.
* Final_sprint.pdf
	* The end presentation of this project.
* Intermediate_report.pdf.
	* The intermediate report of this project.
* poster.png
	* The poster that was used as a demonstration of the project during Thema 11.
* Appendix
	* The folder with the appendix content for the Final report

* **Python scripts:**
This folder contains multiple folders and files with python scripts and test data to test the created functions separately. The functions in these scripts are implemented in the pipeline , in the pipeline and in the script you can read how the function works and what it needs as an input.

* **MinionPipeline:**
In this folder you can find the complete script for the pipeline. Details about the functions in the pipeline can be found in the pipeline itself.

* **Readme:**
The readme tells you where to find each file and how the minion pipeline works.

## Prerequisites
- Python3.5 or newer. Python is the scripting language used to create the program.  
https://www.python.org/downloads/

- Albacore is an basecalling program used on the MinIOn data. This tool can be downloaded from the Oxford Nanopore 
website, but an account is needed when logging in. 
https://nanoporetech.com/

- fastqc is a quality control tool for testing the quality of the data. It can be downloaded from the following website
https://www.bioinformatics.babraham.ac.uk/projects/fastqc/

- Porechop is a tool used for trimming the barcodes from the data. 
https://github.com/rrwick/Porechop

- Bowtie2 is a mapping tool used to filter the data. A mapping tool uses negative selection, so when mapping the data
against the human genome and the bee genome, we believe the remains are from DWV.  (as an extra, we perform a blast on 
the data). When using the mapping tool, an index needs to be made to map the data against the human index. It can be 
downloaded from the following website: http://bowtie-bio.sourceforge.net/bowtie2/index.shtml

- Blast is an basic local alignment search tool, used for searching certain organisms in the collected data. 
A blast was used to check for the presence of the DWV in the bee samples. When making use of blast, a database 
needs to be created to blast the data against. This tool can be downloaded from: 
https://blast.ncbi.nlm.nih.gov/Blast.cgi?CMD=Web&PAGE_TYPE=BlastDocs&DOC_TYPE=Download

- Velveloptimiser is an assembly tool used for the assembly of the blast output. This tools also uses Velvet itself.
https://github.com/tseemann/VelvetOptimiser/wiki/VelvetOptimiser-Manual
https://github.com/dzerbino/velvet

- Clustalo is a multiple sequence alignment tool, used to align mulitple sequences against each other. This tool
shows the similarity between sequences and creates an output file, using a fasta format as the default. A guide tree for outputs, can also
be made with Clustalo. This tool can be downloaded from http://www.clustal.org/omega/

- Primer3 is a tool used for creating primers. Primer3 will try to create primers based on the contigs from the 
assembly. Settings can be used as arguments in the commandline, but when no settings are given, the default settings will be used. 
Primer3 returns multiple files with different primer options for each contig. Primer3 can be downloaded from:
https://sourceforge.net/projects/primer3/

## Installation and usage of the pipeline
Before running the pipeline no installation is needed except for the used programs that are listed in the Prerequisities.
For the Primer3 program it is necessary to put the primer_config folder in the self.primer_thermodynamic_parame parameter.

- **Path example:** self.primer_thermodynamic_parame = "/data/storix2/student/Thema12/MinionMinor2018/primer3/primer3-2.4.0/src/primer3_config/"

It's recommended to run the pipeline in an virtual environment where the packages are installed.

- **Command example:** source virtualenv/my_new_app/bin/activate

When the virtual environment is activated the script can be executed. 
Before closing the terminal when the pipeline is finished you have to deactivate the virtual environment. 

- **Command example:** deactivate

The pipeline is runned on the commandline. An input file is necessary and additional options for primer designing can be added but if none is given default settings wil be used.

- **Command example:** python3 MinIONpipeline.py 20180328_1155_first_run/fast5/ (extra options)]

When running this command an directory will be created. In this directory you will find all the output from the different functions in the pipeline.

## Parameters for the pipeline
All possible parameters for this pipeline are listed below. This will also be shown when the pipeline is runned with the --help command. 
The parameters can be added in a row separated by spaces on the command line
For additional information about the parameters please consult the primer3 manual: http://primer3.sourceforge.net/primer3_manual.htm

- **FILE_PATH**
Select a file path that directs to the fast5 folder created by MinION, this parameter is neccessary.

- **PRIMER_TASK**
Pick a primer task, all options below are valid options. For an explanation of the options see the Primer3 Manual. default value: generic
generic, pick_detection_primers, check_primers, pick_primer_list, pick_sequencing_primers, pick_cloning_primers, pick_discriminative_primers, pick_pcr_primers, pick_pcr_primers_and_hyb_probe, pick_left_only, pick_right_only, pick_hyb_probe_only

- **PRIMER_OPT_SIZE**
Insert an optimal primer size. default value: 20

- **PRIMER_MAX_NS_ACCEPTED**
Insert the max number of ns accepted. default value: 1

- **PRIMER_PRODUCT_SIZE_RANGE**
Insert the product size. The product size consists of two numbers with a - in between default value: 75-100

## The project description ##
The decline in bee population is a large problem these days. This problem could be caused by a virus called Deformed 
wing virus (DWV), which is transmitted by mites. This virus was isolated in the lab and is used as data in this project. 
The samples with the isolated virus where converted into data by the use of a device called the minION. For the output 
of this device a pipeline was build that uses multiple programs to research and modify the data and to show DWV 
in the samples. Information about the results can be found in the section down below.

## Results information
- When running the pipeline, an output_pipeline directory with a timestamp is made. Multiple subdirectories are formed 
within this file. 
- An output_albacore directory is created with the fastq files. These files are created by Albacore. 
- A quality control directory is made to test the quality of the albacore output.
- The directory for porechop is made for the trimmed output, and a quality control directory is also created for the porechop output. 
- A blast directory is made with 2 subdirectories where the same files and the 'failed_aligned_reads' are stored. 
- The mapped directory is created for the output from the mapping against the DWV. 
- An assembly directory is created with mulitple subdirectories for filtering on high quality reads, large reads and for the velvet output. 
- The msa directory is created for the clustalo output. 
- The phylogenetic tree directory is created for the vizualisation of the msa file. 
- A primer directory is created with multiple subdirectories for the primer options per contig file. 

## Issues
- The fastqc option hasn't been tested on the minion pc due to installation errors. 
- Blast has running issues so when that happens, the index files needs the be remade in a new directory. 
- The pipeline needs to run in an virtual environment 